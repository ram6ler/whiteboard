library whiteboard_lib;

import "dart:html";
import "dart:math" as math;

part "constants.dart";
part "pen_settings.dart";
part "whiteboard.dart";