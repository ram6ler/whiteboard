part of whiteboard_lib;

/// A helper function for creating [CanvasElement]s.
CanvasElement _createCanvas([String z]) {
  var can = new CanvasElement(width: Page.WIDTH, height: Page.HEIGHT);

  if (z != null) {
    can.style
      ..position = "absolute"
      ..zIndex = z;
  }

  return can;
}

/// The class defining the application object.
class Whiteboard {
  /// The state of the application.
  ///
  /// The response of the application to different events depends
  /// on the current state. [state] can take on the constant values
  /// defined in the abstract class [State].
  int state;

  /// Various container [DivElement]s.
  ///
  /// [parentDiv] contains [boardDiv] and [controlsDiv], which
  /// contain the [CanvasElement]s [display] and [controls] respectively.
  DivElement parentDiv, boardDiv, controlsDiv;

  /// A paragraph to communicate with the user.
  ///
  /// [messenger] mainly lets the user know which page is being viewed,
  /// but could potentially be used for more later...
  ParagraphElement messenger;

  /// The current page index.
  int page;

  /// The user's slides.
  ///
  /// [slide] contains the non erasable work of the user, [notes], the erasable.
  List<CanvasElement> slide, notes;

  /// The offset of each slide.
  ///
  /// Allows support for extending slides in each direction.
  List<int> offsetX, offsetY;

  /// Images to be loaded.
  ImageElement imgPenInterface,
      imgPenBackground,
      imgEraserInterface,
      imgIcons,
      imgControls;

  /// Dynamic images.
  ///
  /// [display] is a "window" onto the current [slide] and [notes] (indexed at [page]).
  /// [scratch] is used to display the incomplete [Path] created by the user before it
  /// is drawn to the current [notes]. [interface] is used to display the settings
  /// menus. [controls] displays the current and the quick settings.
  CanvasElement display, scratch, interface, controls;

  /// The current pen settings.
  PenSettings pen;

  /// The quick settings to be accessed by clicking or hitting the numeric keys.
  List<PenSettings> quickPens;

  /// The path currently being drawn.
  ///
  /// Displayed on [scratch]; when complete, transferred to the current [notes].
  Path2D path;

  /// The initial point.
  ///
  /// Stores where the pen is first pressed down. Necessary for lines, circles,
  /// rectangles.
  Point p0;

  /// Whether the user wants to draw a dot.
  ///
  /// If the user presses the pen down and then lifts it up without moving the pen,
  /// instead of (not) drawing a line, a filled circle is drawn. Useful for dotting i's,
  /// for example.
  bool dot;

  /// Coordinates for pasting images.
  ///
  /// Acts as the centre of a pasted image. Basically copies the mouse onMove coordinates
  /// so that the user has some control over where a pasted image will appear.
  int pasteX, pasteY;

  /// Whether the application is ready to load an image (or still processing a previous load).
  bool loadReady;

  /// Clears a circle of eraser's radius at point [p].
  void erase(Point p) {
    notes[page].context2D
      ..save()
      ..globalCompositeOperation = "destination-out"
      ..beginPath()
      ..arc(
          p.x + offsetX[page], p.y + offsetY[page], pen.radius, 0, 2 * math.pi)
      ..fill()
      ..restore();
    updateDisplay();
  }

  /// Called when the user presses the pen to the canvas.
  ///
  /// How the app responds depends on the current [state].
  void penDown(Point p) {
    switch (state) {
      case State.WRITING_WAITING:
        state = State.WRITING;
        if (pen.mode == Mode.ERASE) {
          erase(p);
        } else {
          path = new Path2D()..moveTo(p.x, p.y);
          p0 = new Point(p.x, p.y);
          dot = true;
        }
        break;
      case State.WRITING:
        break;
      case State.PEN_INTERFACE:
        break;
      case State.LINE_WAITING:
        p0 = new Point(p.x, p.y);
        state = State.LINE;
        break;
      case State.LINE:
        break;
      case State.RECTANGLE_WAITING:
        p0 = new Point(p.x, p.y);
        state = State.RECTANGLE;
        break;
      case State.RECTANGLE:
        break;
      case State.CIRCLE_WAITING:
        p0 = new Point(p.x, p.y);
        state = State.CIRCLE;
        break;
      case State.CIRCLE:
        break;
    }
  }

  /// Called when the user moves the pen while it is pressed against the canvas.
  ///
  /// How the app responds depends on the current [state].
  void penMove(Point p) {
    pasteX = p.x;
    pasteY = p.y;
    switch (state) {
      case State.WRITING_WAITING:
        break;
      case State.WRITING:
        // If the pen moves while down at all, the user probably did not intend
        // to draw a dot. (Note: we can't simply check whether the final coordinates
        // match the initial coordinates because of the rare case in which the user
        // draws a closed path.)
        dot = false;

        if (pen.mode == Mode.ERASE) {
          erase(p);
        } else {
          path.lineTo(p.x, p.y);
          scratch.context2D
            ..lineWidth = pen._width
            ..setLineDash(pen.lineDash)
            ..lineJoin = "round"
            ..lineCap = "round"
            ..strokeStyle = pen.colour
            ..clearRect(0, 0, scratch.width, scratch.height)
            ..stroke(path);
        }
        break;
      case State.PEN_INTERFACE:
        break;
      case State.LINE_WAITING:
        break;
      case State.LINE:
        path = new Path2D()
          ..moveTo(p0.x, p0.y)
          ..lineTo(p.x, p.y);
        scratch.context2D
          ..beginPath()
          ..lineWidth = pen._width
          ..setLineDash(pen.lineDash)
          ..strokeStyle = pen.colour
          ..clearRect(0, 0, scratch.width, scratch.height)
          ..stroke(path)
          ..closePath();
        break;
      case State.RECTANGLE_WAITING:
        break;
      case State.RECTANGLE:
        path = new Path2D()..rect(p0.x, p0.y, p.x - p0.x, p.y - p0.y);
        scratch.context2D
          ..lineWidth = pen._width
          ..setLineDash(pen.lineDash)
          ..lineCap = "round"
          ..strokeStyle = pen.colour
          ..clearRect(0, 0, scratch.width, scratch.height)
          ..stroke(path);
        break;
      case State.CIRCLE_WAITING:
        break;
      case State.CIRCLE:
        path = new Path2D()
          ..arc(
              p0.x,
              p0.y,
              math.sqrt(
                  (p.x - p0.x) * (p.x - p0.x) + (p.y - p0.y) * (p.y - p0.y)),
              0,
              math.pi * 2,
              true);
        scratch.context2D
          ..lineWidth = pen._width
          ..setLineDash(pen.lineDash)
          ..lineCap = "round"
          ..strokeStyle = pen.colour
          ..clearRect(0, 0, scratch.width, scratch.height)
          ..stroke(path);
        break;
    }
  }

  /// Called when the user lifts his pen after it has been pressed to the canvas.
  ///
  /// How the app responds depends on the current [state].
  void penUp(Point p) {
    switch (state) {
      case State.WRITING_WAITING:
        break;
      case State.WRITING:
        state = State.WRITING_WAITING;
        if (pen.mode == Mode.WRITE) {
          if (dot) {
            path = new Path2D()..arc(p.x, p.y, pen.width, 0, 2 * math.pi, true);
            scratch.context2D
              ..fillStyle = pen.colour
              ..clearRect(0, 0, scratch.width, scratch.height)
              ..fill(path);
          }
          pasteBoard(scratch, notes[page]);
          updateDisplay();
        }
        break;
      case State.ERASER_INTERFACE:
        // Selection identification from arbitrary image. See [imgEraserInterface].
        num cx = display.width / 2,
            cy = display.height / 2,
            dist = math.sqrt((cx - p.x) * (cx - p.x) + (cy - p.y) * (cy - p.y)),
            theta = math.atan2(cy - p.y, cx - p.x);

        if (dist < 250 && dist > 110) {
          int index =
              (((theta + 2 * math.pi / 14) / (2 * math.pi / 7)).round() + 3) %
                  7;
          pen
            ..drawState = State.WRITING_WAITING
            ..mode = Mode.ERASE
            ..radius = [5, 10, 20, 30, 40, 50, 60][index];
        }
        updateDisplay();
        break;
      case State.PEN_INTERFACE:
        // Selection identification from arbitrary image. See [imgPenInterface].
        num cx = display.width / 2,
            cy = display.height / 2,
            dist = math.sqrt((cx - p.x) * (cx - p.x) + (cy - p.y) * (cy - p.y)),
            theta = math.atan2(cy - p.y, cx - p.x);
        if (dist < 240 && dist > 165) {
          // Colour selection...
          // Read the colour data of the pixel clicked.

          /* // Can cause "tainted data" error if image not recognised to be from
             // same server...
          List<int> data = display.context2D.getImageData(p.x, p.y, 1, 1).data;
          pen
            ..red = data[0]
            ..green = data[1]
            ..blue = data[2]
            ..mode = Mode.WRITE;*/

          int index = ((theta / (2 * math.pi / 12)).round() + 6) % 12;
          //print("index: $index; dist: $dist");
          num xCent = cx + 195 * math.cos(index * 2 * math.pi / 12),
              yCent = cy + 195 * math.sin(index * 2 * math.pi / 12),
              disCent = math.sqrt((xCent - p.x) * (xCent - p.x) +
                  (yCent - p.y) * (yCent - p.y));

          if (disCent < 25) {
            // coloured circle clicked

            List<int> cols = [
              [180, 225, 0],
              [250, 240, 0],
              [250, 195, 0],
              [245, 155, 0],
              [240, 120, 0],
              [230, 35, 25],
              [220, 0, 145],
              [130, 10, 175],
              [70, 0, 155],
              [10, 80, 220],
              [45, 180, 200],
              [50, 205, 0]
            ][index];

            //print("index: $index; cols: $cols");

            pen
              ..red = cols[0]
              ..green = cols[1]
              ..blue = cols[2]
              ..mode = Mode.WRITE;
          } else {
            // black-grey-white clicked
            if (dist > 218) {
              // black 0
              pen
                ..red = 0
                ..green = 0
                ..blue = 0
                ..mode = Mode.WRITE;
            } else if (dist > 208) {
              // gray 128
              pen
                ..red = 128
                ..green = 128
                ..blue = 128
                ..mode = Mode.WRITE;
            } else if (dist > 198) {
              // darkgray 169
              pen
                ..red = 169
                ..green = 169
                ..blue = 169
                ..mode = Mode.WRITE;
            } else if (dist > 188) {
              //lightgray 211
              pen
                ..red = 211
                ..green = 211
                ..blue = 211
                ..mode = Mode.WRITE;
            } else {
              // white 255
              pen
                ..red = 255
                ..green = 255
                ..blue = 255
                ..mode = Mode.WRITE;
            }
          }
        } else if (dist < 165 && dist > 120) {
          // Pen width selection...
          int index = (theta / (2 * math.pi / 9)).round() + 4,
              selectedWidth = [1, 2, 3, 4, 5, 10, 15, 25, 32][index];
          pen
            ..width = selectedWidth
            ..mode = Mode.WRITE;
        } else if (dist < 120 && dist > 80) {
          // Dash type selection...
          int index = ((theta + math.pi / 4) / (2 * math.pi / 4)).round() + 1,
              selectedDash = [
            LineType.SOLID,
            LineType.DASH,
            LineType.DOT,
            LineType.DASH_DOT
          ][index];
          pen
            ..updateLineType(selectedDash)
            ..mode = Mode.WRITE;
        } else if (dist < 80) {
          // Alpha and shape selection
          for (int i = 0; i < 6; i++) {
            num th = i * 2 * math.pi / 6,
                x = cx + 60 * math.cos(th),
                y = cy + 60 * math.sin(th),
                d = math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));

            if (d < 12) {
              pen
                ..alpha = [0.1, 0.2, 0.3, 0.5, 0.7, 0.9][i]
                ..mode = Mode.WRITE;
              break;
            }
          }

          for (int i = 0; i < 4; i++) {
            num th = i * 2 * math.pi / 4,
                x = cx + 25 * math.cos(th),
                y = cy + 25 * math.sin(th),
                d = math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));

            if (d < 12) {
              pen
                ..drawState = [
                  State.WRITING_WAITING,
                  State.LINE_WAITING,
                  State.RECTANGLE_WAITING,
                  State.CIRCLE_WAITING
                ][i]
                ..mode = Mode.WRITE;

              break;
            }
          }
        }
        updateDisplay();
        break;
      case State.LINE_WAITING:
        break;
      case State.LINE:
        state = State.LINE_WAITING;
        pasteBoard(scratch, notes[page]);
        updateDisplay();
        break;
      case State.RECTANGLE_WAITING:
        break;
      case State.RECTANGLE:
        state = State.RECTANGLE_WAITING;
        pasteBoard(scratch, notes[page]);
        updateDisplay();
        break;
      case State.CIRCLE_WAITING:
        break;
      case State.CIRCLE:
        state = State.CIRCLE_WAITING;
        pasteBoard(scratch, notes[page]);
        updateDisplay();
        break;
    }
  }

  /// Draws one [CanvasElement] onto another and clears it.
  void pasteBoard(CanvasElement from, CanvasElement to) {
    to.context2D.drawImageToRect(from,
        new Rectangle(offsetX[page], offsetY[page], from.width, from.height));
    from.context2D.clearRect(0, 0, from.width, from.height);
  }

  /// Draws the "window" on the current [slide] and [notes] or shows an interface.
  void updateDisplay([String message]) {
    if (message == null) message = "Page ${page + 1} of ${slide.length}.";
    messenger.innerHtml = message;
    display.context2D..clearRect(0, 0, display.width, display.height);

    switch (state) {
      case State.ERASER_INTERFACE:
        display.context2D
          // Draw the slide as a faded background...
          ..drawImageToRect(
              slide[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height))
          ..drawImageToRect(
              notes[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height))
          ..fillStyle = "rgba(255,255,255,0.85)"
          ..fillRect(0, 0, display.width, display.height)
          // Show the eraser interface
          ..drawImage(
              imgEraserInterface,
              (display.width - imgEraserInterface.width) / 2,
              (display.height - imgEraserInterface.height) / 2);

        break;
      case State.PEN_INTERFACE:
        display.context2D
          // Draw the slide as a faded background...
          ..drawImageToRect(
              slide[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height))
          ..drawImageToRect(
              notes[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height))
          ..fillStyle = "rgba(255,255,255,0.85)"
          ..fillRect(0, 0, display.width, display.height)
          // Show the pen interface
          ..drawImage(
              imgPenInterface,
              (display.width - imgPenInterface.width) / 2,
              (display.height - imgPenInterface.height) / 2)
          ..drawImage(
              imgPenBackground,
              (display.width - imgPenBackground.width) / 2,
              (display.height + imgPenInterface.height) / 2)
          ..beginPath()
          ..lineWidth = pen._width
          ..setLineDash(pen.lineDash)
          ..lineCap = "round"
          ..strokeStyle = pen.colour
          ..moveTo(
              (display.width - imgPenBackground.width) / 2,
              (display.height +
                      imgPenInterface.height +
                      imgPenBackground.height) /
                  2)
          ..lineTo(
              (display.width + imgPenBackground.width) / 2,
              (display.height +
                      imgPenInterface.height +
                      imgPenBackground.height) /
                  2)
          ..stroke()
          ..closePath();
        break;
      case State.WRITING_WAITING:
      case State.WRITING:
      case State.LINE_WAITING:
      case State.LINE:
      case State.RECTANGLE_WAITING:
      case State.RECTANGLE:
      case State.CIRCLE_WAITING:
      case State.CIRCLE:
        display.context2D
          // Draw the slide
          ..drawImageToRect(
              slide[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height))
          ..drawImageToRect(
              notes[page], new Rectangle(0, 0, display.width, display.height),
              sourceRect: new Rectangle(
                  offsetX[page], offsetY[page], display.width, display.height));
        break;
    }

    showSettings();
  }

  /// Extends the board in a direction for when the user runs out of space.
  void extendBoard(int direction) {
    int width, height, x, y;

    switch (direction) {
      case Direction.LEFT:
        width = slide[page].width + Page.CHANGE;
        height = slide[page].height;
        x = Page.CHANGE;
        y = 0;
        offsetX[page] = 0;
        break;
      case Direction.RIGHT:
        width = slide[page].width + Page.CHANGE;
        height = slide[page].height;
        x = 0;
        y = 0;
        offsetX[page] = width - display.width;
        break;
      case Direction.UP:
        width = slide[page].width;
        height = slide[page].height + Page.CHANGE;
        x = 0;
        y = Page.CHANGE;
        offsetY[page] = 0;
        break;
      case Direction.DOWN:
        width = slide[page].width;
        height = slide[page].height + Page.CHANGE;
        x = 0;
        y = 0;
        offsetY[page] = height - display.height;
        break;
    }

    CanvasElement tempSlide = new CanvasElement(width: width, height: height),
        tempNotes = new CanvasElement(width: width, height: height);

    tempSlide.context2D.drawImageToRect(slide[page],
        new Rectangle(x, y, slide[page].width, slide[page].height));

    tempNotes.context2D.drawImageToRect(notes[page],
        new Rectangle(x, y, notes[page].width, notes[page].height));

    slide[page] = tempSlide;
    notes[page] = tempNotes;

    updateDisplay();
  }

  /// Scroll the "window" over the current [slide] and [notes].
  void moveDisplay(int direction) {
    switch (direction) {
      case Direction.UP:
        if (offsetY[page] > 0) offsetY[page] -= Page.NUDGE;
        break;
      case Direction.DOWN:
        if (offsetY[page] < slide[page].height - display.height)
          offsetY[page] += Page.NUDGE;
        break;
      case Direction.LEFT:
        if (offsetX[page] > 0) offsetX[page] -= Page.NUDGE;
        break;
      case Direction.RIGHT:
        if (offsetX[page] < slide[page].width - display.width)
          offsetX[page] += Page.NUDGE;
        break;
    }

    updateDisplay();
  }

  /// Insert a new page at the current index, [page].
  void insertBoard() {
    if (slide.length == 0) {
      // Initialise first page
      slide.add(_createCanvas());
      notes.add(_createCanvas());
      offsetX.add(0);
      offsetY.add(0);
    } else {
      // Insert page
      slide.insert(page + 1, _createCanvas());
      notes.insert(page + 1, _createCanvas());
      offsetX.insert(page + 1, 0);
      offsetY.insert(page + 1, 0);
      page++;
    }

    updateDisplay();
  }

  /// Delete the current [slide] and [notes].
  void deleteBoard() {
    slide.removeAt(page);
    notes.removeAt(page);
    offsetX.removeAt(page);
    offsetY.removeAt(page);
    if (slide.length == 0) {
      insertBoard();
    } else if (page > 0) {
      page--;
    }
    updateDisplay();
  }

  /// Shift focus to the previous [slide] and [notes].
  void previousBoard() {
    if (page > 0) {
      page--;
      updateDisplay();
    }
  }

  /// Shift focus to the next [slide] and [notes].
  void nextBoard() {
    if (page < slide.length - 1) {
      page++;
      updateDisplay();
    }
  }

  /// Load image files selected by user.
  ///
  /// Images are set up as (non erasable) [slide]s over which [notes] can be added.
  /// Called when the user loads files via the load control and in response to the
  /// user drag-and-dropping files over the app.
  void loadFiles(List<File> fileList) {
    loadReady = false;
    int index = 0;

    var reader = new FileReader();
    reader.onLoad.listen((_) {
      String uri = reader.result;
      // Only accept an image file.
      if (uri.substring(0, 20).contains("image")) {
        insertBoard();
        var img = new ImageElement(src: uri);
        img.onLoad.listen((Event e) {
          /*
          // If the image is too big, scale down...
          num w = img.width,
              h = img.height;

          if (w > Page.WIDTH) {
            h *= (Page.WIDTH / w);
            w *= (Page.WIDTH / w);
          }

          if (h > Page.HEIGHT) {
            w *= (Page.HEIGHT / h);
            h *= (Page.HEIGHT / h);
          }

          slide[page].context2D.drawImageToRect(img, new Rectangle(
              (Page.WIDTH - img.width) / 2, (Page.HEIGHT - img.height) / 2,
              img.width, img.height));
          */

          // If the image is too big, increase the size of the slide...
          if (img.width > slide[page].width ||
              img.height > slide[page].height) {
            slide[page] = new CanvasElement(
                width: math.max(img.width, slide[page].width),
                height: math.max(img.height, slide[page].height));
          }
          slide[page].context2D.drawImageToRect(
              img,
              new Rectangle(
                  (slide[page].width - img.width) / 2,
                  (slide[page].height - img.height) / 2,
                  img.width,
                  img.height));

          index++;

          if (index < fileList.length) {
            // When finished working with one image, read the next.
            reader.readAsDataUrl(fileList[index]);
          } else {
            loadReady = true;
            updateDisplay();
          }
        });
      }
    });

    // Read the first image.
    reader.readAsDataUrl(fileList[index]);
  }

  /// Opens the browser's dialogue for uploading a file.
  ///
  /// This is done by creating a temporary file [InputElement], binding the
  /// behaviour to an onChange event and simulating a click to initiate the
  /// response.
  void setupLoader() {
    new InputElement(type: "file")
      ..multiple = true
      ..onChange.listen((Event e) {
        if (loadReady) loadFiles((e.target as InputElement).files);
      })
      ..click();
  }

  /// Saves the current [slide] and [notes] as a png.
  ///
  /// Attaches the data to a temporary [AnchorElement] and simulates a click
  /// to initiate the download.
  void setupSaver() {
    var can =
        new CanvasElement(width: slide[page].width, height: slide[page].height);
    can.context2D
      ..fillStyle = "white"
      ..fillRect(0, 0, can.width, can.height)
      ..drawImage(slide[page], 0, 0)
      ..drawImage(notes[page], 0, 0);

    new AnchorElement(
        href: can
            .toDataUrl("image/png", 1.0)
            .replaceFirst("image/png", "image/octet-stream"))
      ..download = "page${page + 1}.png"
      ..click();
  }

  /// Displays the current and quick pen settings.
  void showSettings() {
    controls.context2D..drawImage(imgControls, 0, 0);

    Function drawAt = (int r, PenSettings p) {
      if (p.mode == Mode.ERASE) {
        controls.context2D
          ..drawImageToRect(imgIcons, new Rectangle(0, r, 32, 32),
              sourceRect: new Rectangle(5 * 32, 0, 32, 32))
          ..save()
          ..strokeStyle = "lightgray"
          ..lineWidth = 30
          ..beginPath()
          ..moveTo(32, r + 16)
          ..lineTo(64, r + 16)
          ..stroke()
          ..closePath()
          ..fillStyle = "white"
          ..beginPath()
          ..arc(32 + 16, r + 16, p.radius / 60 * 15, 0, math.pi * 2)
          ..closePath()
          ..fill()
          ..restore();
      } else {
        switch (p.drawState) {
          case State.WRITING_WAITING:
            controls.context2D
              ..drawImageToRect(imgIcons, new Rectangle(0, r, 32, 32),
                  sourceRect: new Rectangle(0 * 32, 0, 32, 32));
            break;
          case State.LINE_WAITING:
            controls.context2D
              ..drawImageToRect(imgIcons, new Rectangle(0, r, 32, 32),
                  sourceRect: new Rectangle(1 * 32, 0, 32, 32));
            break;
          case State.RECTANGLE_WAITING:
            controls.context2D
              ..drawImageToRect(imgIcons, new Rectangle(0, r, 32, 32),
                  sourceRect: new Rectangle(2 * 32, 0, 32, 32));
            break;
          case State.CIRCLE_WAITING:
            controls.context2D
              ..drawImageToRect(imgIcons, new Rectangle(0, r, 32, 32),
                  sourceRect: new Rectangle(3 * 32, 0, 32, 32));
            break;
        }

        String penWidth = "${p.width.toInt().toString()}pt",
            penStroke = ["SOLID", "DASH", "DOT", "MIXED"][p.lineType];

        controls.context2D
          ..save()
          ..strokeStyle = "black"
          ..lineWidth = 2
          ..beginPath()
          ..moveTo(32, r + 16)
          ..lineTo(64, r + 16)
          ..stroke()
          ..closePath()
          ..fillStyle = p.colour
          ..beginPath()
          ..arc(32 + 16, r + 16, 8, 0, math.pi * 2)
          ..closePath()
          ..fill()
          ..fillStyle = "black"
          ..textAlign = "center"
          ..font = "8pt Tahoma"
          ..fillText(penWidth, 32 + 16, r + 9)
          ..fillText(penStroke, 32 + 16, r + 32 - 1)
          ..restore();
      }
    };

    // Current pen settings...
    drawAt(85 * 32 ~/ 10, pen);

    // Quick pen settings...
    for (int i = 0; i < 10; i++) {
      drawAt((i + 10) * 32, quickPens[i]);
    }
  }

  /// Selects or saves one of the quick pen settings.
  ///
  /// Called in response to a controls click or a keyboard event. If one of the
  /// interfaces is open, the current settings are saved into the quick pen
  /// settings at index [qickIndex]; otherwise, the quick pen settings are
  /// saved into the current settings.
  void quickSelect(int quickIndex) {
    if (state == State.ERASER_INTERFACE || state == State.PEN_INTERFACE) {
      quickPens[quickIndex].copyFrom(pen);
    } else {
      pen.copyFrom(quickPens[quickIndex]);
      state = pen.drawState;
    }
    updateDisplay();
  }

  /// Sets the dash-type of the [pen].
  ///
  /// Called in response to the user hitting a shortcut key rather than opening the
  /// pen interface, selecting the dash-type and closing the interface.
  void quickShape(int key) {
    if (state != State.ERASER_INTERFACE &&
        state != State.PEN_INTERFACE &&
        pen.mode == Mode.WRITE) {
      if (key == KeyCode.P) {
        // P for "pen"...
        pen.drawState = State.WRITING_WAITING;
      } else if (key == KeyCode.L) {
        // L for "line"...
        pen.drawState = State.LINE_WAITING;
      } else if (key == KeyCode.C) {
        // C for "circle"...
        pen.drawState = State.CIRCLE_WAITING;
      } else if (key == KeyCode.R) {
        // R for "rectangle"...
        pen.drawState = State.RECTANGLE_WAITING;
      }
      state = pen.drawState;
      updateDisplay();
    }
  }

  /// The whiteboard application.
  ///
  /// A single instance should be created in the body of main.
  Whiteboard() {
    // The default pen settings...
    pen = new PenSettings(
        mode: 0,
        red: 10,
        green: 80,
        blue: 220,
        alpha: 0.9,
        width: 2,
        radius: 8.0,
        drawState: 0,
        lineType: 0);

    // The default quick pen settings...
    // (Hitting Shift+D while using the application will cause the user's pen settings
    // to be printed out to the JS console; it can be copied and pasted between the brackets
    // below.)
    quickPens = [
      new PenSettings(
          mode: 0,
          red: 10,
          green: 80,
          blue: 220,
          alpha: 0.9,
          width: 2,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 230,
          green: 35,
          blue: 25,
          alpha: 0.9,
          width: 2,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 50,
          green: 205,
          blue: 0,
          alpha: 0.9,
          width: 2,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 0,
          green: 0,
          blue: 0,
          alpha: 0.9,
          width: 2,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 250,
          green: 240,
          blue: 0,
          alpha: 0.3,
          width: 32,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 245,
          green: 155,
          blue: 0,
          alpha: 0.3,
          width: 32,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 130,
          green: 10,
          blue: 175,
          alpha: 0.3,
          width: 32,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 0,
          red: 128,
          green: 128,
          blue: 128,
          alpha: 0.7,
          width: 2,
          radius: 8.0,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 1,
          red: 130,
          green: 10,
          blue: 175,
          alpha: 0.5,
          width: 32,
          radius: 10,
          drawState: 0,
          lineType: 0),
      new PenSettings(
          mode: 1,
          red: 130,
          green: 10,
          blue: 175,
          alpha: 0.5,
          width: 32,
          radius: 60,
          drawState: 0,
          lineType: 0)
    ];

    state = pen.drawState;

    path = new Path2D();

    p0 = new Point(0, 0);

    page = 0;

    pasteX = pasteY = 0;

    dot = false;

    loadReady = true;

    // Load the images needed...
    imgPenInterface = new ImageElement(src: "images/PenMenu.png")
      ..onLoad.listen((_) {
        imgPenBackground = new ImageElement(src: "images/PenBackground.png")
          ..onLoad.listen((_) {
            imgEraserInterface = new ImageElement(
                src: "images/EraserSelect.png")
              ..onLoad.listen((_) {
                imgIcons = new ImageElement(src: "images/PenLineCircleRect.png")
                  ..onLoad.listen((_) {
                    imgControls = new ImageElement(src: "images/controls.png")
                      ..onLoad.listen((_) {
                        // After all the images are loaded, start the app by inserting a board.
                        insertBoard();
                        new ImageElement(src: "images/title.png")
                          ..onLoad.listen((Event e) {
                            ImageElement img = e.target;
                            slide[page].context2D.drawImage(
                                img,
                                (Page.WIDTH - img.width) / 2,
                                (Page.HEIGHT - img.height) / 2);
                            updateDisplay();
                          });
                      });
                  });
              });
          });
      });

    // The messenger displays which page we're on...
    messenger = new ParagraphElement();
    messenger.style
      ..fontSize = "10pt"
      ..fontFamily = "Tahoma"
      ..width = "${Page.WIDTH + 64}px"
      ..marginLeft = "auto"
      ..marginRight = "auto";

    slide = new List<CanvasElement>();
    notes = new List<CanvasElement>();

    offsetX = [0];
    offsetY = [0];
    display = _createCanvas("1");
    scratch = _createCanvas("2");
    interface = _createCanvas("3");
    controls = new CanvasElement(width: 64, height: 20 * 32);

    parentDiv = new DivElement();
    boardDiv = new DivElement();
    controlsDiv = new DivElement();

    // Avoid the user selecting components when attempting to draw...
    document.body.style.userSelect = "none";

    // Center [parentDiv]...
    parentDiv.style
      ..width = "${Page.WIDTH + 64 + 4}px"
      ..height = "${Page.HEIGHT}px"
      ..marginLeft = "auto"
      ..marginRight = "auto";

    // Position the drawing board and controls...
    boardDiv.style
      ..width = "${Page.WIDTH}px"
      ..height = "${Page.HEIGHT}px"
      ..float = "right"
      ..border = "1pt solid lightgray";

    controlsDiv.style
      ..width = "64px"
      ..height = "${Page.HEIGHT}px"
      ..float = "left";

    controlsDiv.children.add(controls);

    parentDiv.children.addAll([controlsDiv, boardDiv]);

    // Respond to mouse and touch events...
    boardDiv
      ..onMouseDown.listen((MouseEvent me) {
        penDown(me.offset);
      })
      ..onMouseMove.listen((MouseEvent me) {
        penMove(me.offset);
      })
      ..onMouseUp.listen((MouseEvent me) {
        penUp(me.offset);
      })
      // TODO: Handle leave, enter events...
      ..onMouseLeave.listen((MouseEvent me) {
        penUp(me.offset);
      })
      ..onMouseEnter.listen((MouseEvent me) {})
      // TODO: Test touch events...
      ..onTouchStart.listen((TouchEvent te) {
        var p = new Point(te.touches[0].page.x - boardDiv.offsetLeft,
            te.touches[0].page.y - boardDiv.offsetTop);
        penDown(p);
      })
      ..onTouchMove.listen((TouchEvent te) {
        var p = new Point(te.touches[0].page.x - boardDiv.offsetLeft,
            te.touches[0].page.y - boardDiv.offsetTop);
        penMove(p);
      })
      ..onTouchEnd.listen((TouchEvent te) {
        var p = new Point(te.changedTouches[0].page.x - boardDiv.offsetLeft,
            te.changedTouches[0].page.y - boardDiv.offsetTop);
        penUp(p);
      })
      // TODO: Handle leave, enter events...
      ..onTouchLeave.listen((TouchEvent te) {
        var p = new Point(te.changedTouches[0].page.x - boardDiv.offsetLeft,
            te.changedTouches[0].page.y - boardDiv.offsetTop);
        penUp(p);
      })
      ..onTouchEnter.listen((TouchEvent te) {});

    // Make the controls interactive...
    controlsDiv
      ..onClick.listen((MouseEvent me) {
        int index = me.offset.y ~/ 32 * 2 + me.offset.x ~/ 32;

        switch (index) {
          case Controls.LOAD:
            setupLoader();
            break;
          case Controls.SAVE:
            setupSaver();
            break;
          case Controls.ADD_PAGE:
            insertBoard();
            break;
          case Controls.DELETE_PAGE:
            deleteBoard();
            break;
          case Controls.PREVIOUS_PAGE:
            previousBoard();
            break;
          case Controls.NEXT_PAGE:
            nextBoard();
            break;
          case Controls.ADD_LEFT:
            extendBoard(Direction.LEFT);
            break;
          case Controls.ADD_RIGHT:
            extendBoard(Direction.RIGHT);
            break;
          case Controls.ADD_UP:
            extendBoard(Direction.UP);
            break;
          case Controls.ADD_DOWN:
            extendBoard(Direction.DOWN);
            break;
          case Controls.NUDGE_LEFT:
            moveDisplay(Direction.LEFT);
            break;
          case Controls.NUDGE_RIGHT:
            moveDisplay(Direction.RIGHT);
            break;
          case Controls.NUDGE_UP:
            moveDisplay(Direction.UP);
            break;
          case Controls.NUDGE_DOWN:
            moveDisplay(Direction.DOWN);
            break;
          case Controls.PEN_SETTINGS:
            if (state == State.PEN_INTERFACE) {
              state = pen.drawState;
            } else {
              state = State.PEN_INTERFACE;
            }
            updateDisplay();
            break;
          case Controls.ERASER_SETTINGS:
            if (state == State.ERASER_INTERFACE) {
              state = pen.drawState;
            } else {
              state = State.ERASER_INTERFACE;
            }

            updateDisplay();
            break;
          case Controls.ZERO:
          case Controls.ONE:
          case Controls.TWO:
          case Controls.THREE:
          case Controls.FOUR:
          case Controls.FIVE:
          case Controls.SIX:
          case Controls.SEVEN:
          case Controls.EIGHT:
          case Controls.NINE:
            int quickIndex = Controls.QUICK_SELECTION(index);
            quickSelect(quickIndex);
            break;
          default:
            break;
        }
      });

    boardDiv.children.addAll([display, scratch, interface]);

    // Create a link to the repo on BitBucket...
    var bbImage = new ImageElement()..src = "images/bitbucket.png";
    bbImage.style
      ..width = "50px"
      ..float = "right";
    var bbLink = new AnchorElement()
      ..href = "https://bitbucket.org/ram6ler/whiteboard/wiki/Home"
      ..target = "_blank";
    bbLink.children.add(bbImage);

    document.body.children.addAll([parentDiv, messenger, bbLink]);

    // Keyboard shortcuts...
    document
      ..onKeyDown.listen((KeyboardEvent ke) {
        switch (ke.keyCode) {
          case KeyCode.F:
            if (ke.shiftKey) {
              // Shift + F requests fullscreen...
              document.body.requestFullscreen();
            }
            break;
          case KeyCode.D:
            if (ke.shiftKey) {
              for (PenSettings p in quickPens) {
                // Shift + D prints the users settings to the JS console, which can be
                // pasted as initial quick pen settings. (See comments where [quickPens]
                // is initiated above.)
                print(p);
              }
            }
            break;
          // Set or save quick pen settings...
          case KeyCode.ZERO:
            quickSelect(0);
            break;
          case KeyCode.ONE:
            quickSelect(1);
            break;
          case KeyCode.TWO:
            quickSelect(2);
            break;
          case KeyCode.THREE:
            quickSelect(3);
            break;
          case KeyCode.FOUR:
            quickSelect(4);
            break;
          case KeyCode.FIVE:
            quickSelect(5);
            break;
          case KeyCode.SIX:
            quickSelect(6);
            break;
          case KeyCode.SEVEN:
            quickSelect(7);
            break;
          case KeyCode.EIGHT:
            quickSelect(8);
            break;
          case KeyCode.NINE:
            quickSelect(9);
            break;
          // Quickly select pen, line, circle or rectangle mode...
          case KeyCode.P:
          case KeyCode.L:
          case KeyCode.C:
          case KeyCode.R:
            quickShape(ke.keyCode);
            break;
          // Scroll around or extend boards (+ Shift) with the arrow keys...
          case KeyCode.UP:
            if (ke.shiftKey)
              extendBoard(Direction.UP);
            else
              moveDisplay(Direction.UP);
            break;
          case KeyCode.DOWN:
            if (ke.shiftKey)
              extendBoard(Direction.DOWN);
            else
              moveDisplay(Direction.DOWN);
            break;
          case KeyCode.LEFT:
            if (ke.shiftKey)
              extendBoard(Direction.LEFT);
            else
              moveDisplay(Direction.LEFT);
            break;
          case KeyCode.RIGHT:
            if (ke.shiftKey)
              extendBoard(Direction.RIGHT);
            else
              moveDisplay(Direction.RIGHT);
            break;
        }
      })
      ..onKeyUp.listen((KeyboardEvent ke) {})
      // Handle an image being pasted...
      ..onPaste.listen((e) {
        Blob blob = e.clipboardData.items[0].getAsFile();
        new FileReader()
          ..onLoad.listen((ProgressEvent pe) {
            var img = new ImageElement(src: (pe.target as FileReader).result);
            img.onLoad.listen((_) {
              if (!State.ALL_WAITING.contains(state)) {
                slide[page].context2D.drawImage(
                    img,
                    offsetX[page] + pasteX - img.width / 2,
                    offsetY[page] + pasteY - img.height / 2);
                updateDisplay();
              } else {
                // resize if necessary
                num factorW = 1.0, factorH = 1.0;
                if (img.width > slide[page].width)
                  factorW = slide[page].width / img.width;
                if (img.height > slide[page].height)
                  factorH = slide[page].height / img.height;

                num factor = math.min(factorW, factorH),
                    width = img.width * factor,
                    height = img.height * factor;

                slide[page].context2D.drawImageToRect(
                    img,
                    new Rectangle((slide[page].width - width) / 2,
                        (slide[page].height - height) / 2, width, height));
                updateDisplay();
              }
            });
          })
          ..readAsDataUrl(blob);
      })
      // Prevent the browser from opening an image that is dragged over and dropped...
      ..onDragOver.listen((MouseEvent me) {
        me
          ..preventDefault()
          ..stopPropagation();
      })
      ..onDrop.listen((MouseEvent me) {
        me
          ..preventDefault()
          ..stopPropagation();
        loadFiles(me.dataTransfer.files);
      });
  }
}
