part of whiteboard_lib;

/// The settings of a pen to be used on a [Whiteboard].
class PenSettings {
  int
      /// Whether the pen is used to write (Mode.WRITE) or
      /// to erase (Mode.ERASE).
      mode,
      /// The red value in the RGB colour scheme (0 - 255).
      red,
      /// The green value in the RGB colour scheme (0 - 255).
      green,
      /// The blue value in the RGB colour scheme (0 - 255).
      blue,
      /// The line type (LineType.DASH, LineType.DASH_DOT,
      /// LineType.DOT or LineType.SOLID).
      lineType,
      /// The shape-state that the pen will draw (e.g.
      /// State.CIRCLE_WAITING).
      drawState;

  num
      /// The alpha setting.
      alpha,
      /// The radius of the eraser
      radius,
      _width;

  /// The width of the pen.
  void set width(num x) {
    _width = x;
    updateLineType(lineType);
  }

  num get width => _width;

  /// The lengths of line segments and gaps.
  ///
  /// To be passed to `setLineDash` method of a [CanvasRenderingContext2D].
  List<num> lineDash;

  /// A string representation of the colour and alpha settings.
  ///
  /// To be used to set `strokeStyle` or `fillStyle` properties
  /// of a [CanvasRenderingContext2D].
  String get colour => "rgba($red,$green,$blue,$alpha)";

  /// Update the dash-type.
  void updateLineType(int lineType) {
    num dash = math.max(4 * _width, 12),
        gap = math.max(1.5 * _width, 4),
        dot = math.max(1.5 * _width, 4);
    this.lineType = lineType;
    switch (lineType) {
      case LineType.SOLID:
        lineDash = [];
        break;
      case LineType.DASH:
        lineDash = [dash, gap];
        break;
      case LineType.DOT:
        lineDash = [dot, gap];
        break;
      case LineType.DASH_DOT:
        lineDash = [dash, gap, dot, gap];
        break;
    }
  }

  /// Copy attributes from another [PenSettings] instance.
  void copyFrom(PenSettings p) {
    this
      ..mode = p.mode
      ..red = p.red
      ..green = p.green
      ..blue = p.blue
      ..alpha = p.alpha
      ..width = p.width
      ..radius = p.radius
      ..drawState = p.drawState;
    updateLineType(p.lineType);
  }

  /// Instantiate a [Pensettings] object with attribute values.
  PenSettings({int mode: Mode.WRITE, int red: 200, int green: 0, int blue: 120,
      num alpha: 0.8, int lineType: LineType.DASH_DOT, num width: 2.0,
      num radius: 8.0, int drawState: State.CIRCLE_WAITING}) {
    this
      ..mode = mode
      ..red = red
      ..green = green
      ..blue = blue
      ..alpha = alpha
      ..width = width
      ..radius = radius
      ..drawState = drawState;
    updateLineType(lineType);
  }

  // Used to print out the settings in a manner that can be easily copied
  // and inserted into the code...
  @override
  toString() => (new StringBuffer()
    ..writeAll([
      "new PenSettings(\n",
      "  mode: $mode,\n",
      "  red: $red,\n",
      "  green: $green,\n",
      "  blue: $blue,\n",
      "  alpha: $alpha,\n",
      "  width: $width,\n",
      "  radius: $radius,\n",
      "  drawState: $drawState,\n",
      "  lineType: $lineType\n",
      "),"
    ])).toString();
}
