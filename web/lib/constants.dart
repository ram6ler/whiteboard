part of whiteboard_lib;

abstract class Page {
  static const int WIDTH = 768,
      HEIGHT = 640,
      CHANGE = 50,
      NUDGE = 10;
}

abstract class Direction {
  static const int UP = 0,
      DOWN = 1,
      LEFT = 2,
      RIGHT = 3;
}

abstract class LineType {
  static const int SOLID = 0,
      DASH = 1,
      DOT = 2,
      DASH_DOT = 3;
}

abstract class State {
  static const int WRITING_WAITING = 0,
      WRITING = 1,
      ERASER_INTERFACE = 2,
      PEN_INTERFACE = 3,
      LINE_WAITING = 4,
      LINE = 5,
      RECTANGLE_WAITING = 6,
      RECTANGLE = 7,
      CIRCLE_WAITING = 8,
      CIRCLE = 9;

  static final List ALL_WAITING = [
    WRITING_WAITING,
    LINE_WAITING,
    RECTANGLE_WAITING,
    CIRCLE_WAITING
  ];
}

abstract class Controls {
  static const int LOAD = 0,
      SAVE = 1,
      ADD_PAGE = 2,
      DELETE_PAGE = 3,
      PREVIOUS_PAGE = 4,
      NEXT_PAGE = 5,
      ADD_LEFT = 6,
      ADD_RIGHT = 7,
      ADD_UP = 8,
      ADD_DOWN = 9,
      NUDGE_LEFT = 10,
      NUDGE_RIGHT = 11,
      NUDGE_UP = 12,
      NUDGE_DOWN = 13,
      PEN_SETTINGS = 14,
      ERASER_SETTINGS = 15,
      ZERO = 20,
      ONE = 22,
      TWO = 24,
      THREE = 26,
      FOUR = 28,
      FIVE = 30,
      SIX = 32,
      SEVEN = 34,
      EIGHT = 36,
      NINE = 38;

  static int QUICK_SELECTION(int x) => (x - 20) ~/ 2;
}

abstract class Mode {
  static const int WRITE = 0,
      ERASE = 1;
}
