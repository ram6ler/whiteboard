# Whiteboard

Welcome to _Whiteboard_, a simple pen-and-tablet app for presenting notes to a class.

Please see the [project wiki](https://bitbucket.org/ram6ler/whiteboard/wiki/Home) for more information.

[Click here to try out the app in your browser.](http://whiteboard.aerobatic.io/)
